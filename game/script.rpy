﻿# Coloca el código de tu juego en este archivo.

# Declara los personajes usados en el juego como en el ejemplo:

define e = Character("Eileen")
define al = Character("Alejandro")
define so = Character("Sofía")
define na = Character("Narrador")
define mt = Character("Máquina del tiempo")
define pl = Character("Polemarca ateniense")
define cg = Character("CENIT-GEN")
define cp = Character("Campesino")
define rg = Character("Rodrigo de Cervantes")
define hm = Character("Hombre misterioso")
init: 
    
   $ nombre_objetos = ["Dólares","Pistola","Teléfono móvil","Puntos de habilidad",]
   $ cantidad_objetos = [600,0,1,0]
   $ a = 0
   $ b = 0
   $ c = 0
    
screen inventario:
    frame xpos 0 ypos 0:
       vbox:
          for a in range (0,len(nombre_objetos)):
             if cantidad_objetos[a] > 0:
                $ b = nombre_objetos[a]
                $ c = cantidad_objetos[a]
                text "[b]: [c]" xalign 0.1 

# Imágenes usadas

image intelagency = "agencia-de-inteligencia.jpg"

# El juego comienza aquí.

label start:

    # Muestra una imagen de fondo:

    scene intelagency

    # Muestra un personaje:

    show eileen happy
    
    show screen inventario
    

    # Presenta las líneas del diálogo.

    e "Vamos a probar el inventario. Coge la pistola"
    
    $ cantidad_objetos [1] += 1

    e "Perfecto. Ahora dame 200 dólares."
    
    al "Venga. Con 100 te conformas."
    
    $ cantidad_objetos [0] -= 100

    e "Perfecto."

    jump capitulo4

    # Finaliza el juego:

    return
